//https://scotch.io/tutorials/use-ejs-to-template-your-node-application
//add all requirements
const express = require ('express');

const app = express();

//set the view engine to ejs
app.set('view engine', 'ejs');

// use res.render to load up an ejs view file

//render index.ejs
app.get('/', function(request, response){
  response.render('pages/index');
})

//render about.ejs
app.get('/about', function(request, response){
  response.render('pages/about');
})

//render contact.ejs
app.get('/contact', function(request, response){
  response.render('pages/contact');
})

//render services.ejs
app.get('/services', function(request, response){
  response.render('pages/services');
})

//set the port to listen to
app.listen(8080);
console.log('Server started on port 8080');